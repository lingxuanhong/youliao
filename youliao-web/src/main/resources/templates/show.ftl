﻿﻿<html xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8"/>
    <script type="text/javascript" src="/api/js/X-doc.js"></script>
</head>
<body style="height:100%; margin:0; overflow:hidden;">
<script id="myxdoc" type="text/xdoc" _format="pdf" style="width:100%;height:100%;">
    <xdoc version="11.1.5">
        <meta modifyDate="2017-08-15 14:45:10" author="Administrator" id="3ulahulfbrdwnfujnnjulqjcpm" title="合同预览" createDate="2017-08-15 12:37:46"/>
        <paper width="812" height="1124" topMargin="15" bottomMargin="15" leftMargin="61" rightMargin="71"/>
        <body>
        <para align="center" lineSpacing="2" fillColor="#ff99ff">
            <text fontStyle="bold" fontSize="20">XXXXXX 集 团 有 限 公 司</text>
        </para>
        <para lineSpacing="1">
            <rect color="" name="x5246" width="700">
                <para align="center" lineSpacing="2" fillColor="#ff99ff" name="x5246" width="700">
                    <text fontSize="20"></text>
                    <text fontSize="20"> 调拨单</text>
                    <text fontSize="20"> NO.</text>
                    <text fontSize="20">Y181223000009</text>
                    <text fontSize="15"> 【1-1】</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="1">
            <rect color="" name="x5247" width="350">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x5247" width="350">
                    <text fontSize="14">购货单位:</text>
                    <text fontSize="14">新惠利江苏金龙化肥有限公司</text>
                </para>
            </rect>
            <rect color="" name="x52407" width="120">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x52407" width="120">
                    <text fontSize="14">收货人:</text>
                    <text fontSize="14">严红</text>
                </para>
            </rect>
            <rect color="" name="x52408" width="200">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x52408" width="200">
                    <text fontSize="14">收货人电话:</text>
                    <text fontSize="14">13505221970</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="1">
            <rect color="" name="x5249" width="600">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x5249" width="600">
                    <text fontSize="14">收货地址:</text>
                    <text fontSize="14">江苏省徐州市睢宁县/睢宁县</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="1">
            <rect color="" name="x6870" width="300">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="300">
                    <text fontSize="14">承运单位:</text>
                    <text fontSize="14">XXX物流</text>
                </para>
            </rect>
            <rect color="" name="x6871" width="150">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="150">
                    <text fontSize="14">承运车号:</text>
                    <text fontSize="14">豫NY3297</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="200">
                <para align="center" lineSpacing="3" fillColor="#ff99ff" name="x6872" width="200">
                    <text fontSize="14">承运人:</text>
                    <text fontSize="14">蒋集体</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="1">
            <rect color="" name="x6870" width="300">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="300">
                    <text fontSize="14">运输方式:</text>
                    <text fontSize="14">汽运</text>
                </para>
            </rect>
            <rect color="" name="x6871" width="100">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="100">
                    <text fontSize="14">提货方式:</text>
                    <text fontSize="14">自提</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="280">
                <para align="right" lineSpacing="3" fillColor="#ff99ff" name="x6872" width="280">
                    <text fontSize="14">办单时间:</text>
                    <text fontSize="14">2019-12-03 14:46</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="1">
            <table cellpadding="0" cellspacing="0" height="400" width="700" sizeType="autoheight" name="x342" header="1"
                   cols="75,75,85,75,75,75,75,70,85" rows="">
                <rect col="1" align="center" row="1" colSpan="4" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">品名</text>
                    </para>
                </rect>
                <rect col="5" align="center" row="1" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">规格</text>
                    </para>
                </rect>
                <rect col="7" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">单位</text>
                    </para>
                </rect>
                <rect col="8" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">数量</text>
                    </para>
                </rect>
                <rect col="9" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">产品特性</text>
                    </para>
                </rect>
                <rect col="1" align="center" row="2" colSpan="4" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">粉尘尿素</text>
                    </para>
                </rect>
                <rect col="5" align="center" row="2" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">50Kg</text>
                    </para>
                </rect>
                <rect col="7" align="center" row="2" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">吨</text>
                    </para>
                </rect>
                <rect col="8" align="center" row="2" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">35.00</text>
                    </para>
                </rect>
                <rect col="9" align="center" row="2" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">白分成</text>
                    </para>
                </rect>
                <rect col="1" align="center" row="3" colSpan="4" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">合计（小写）</text>
                    </para>
                </rect>
                <rect col="5" align="center" row="3" colSpan="5" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">T=35.00</text>
                    </para>
                </rect>
            </table>
        </para>
        <para lineSpacing="1">
            <rect color="" name="x6870" width="250">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="250">
                    <text fontSize="14">发货单位盖章:</text>
                    <text fontSize="14"></text>
                </para>
            </rect>
            <rect color="" name="x6871" width="200">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="200">
                    <text fontSize="14">收货人签字:</text>
                    <text fontSize="14"></text>
                </para>
            </rect>
            <rect color="" name="x6872" width="200">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6872" width="200">
                    <text fontSize="14">年 月 日</text>
                    <text fontSize="14"></text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="700">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="600">
                    <text fontSize="14">说明：根据国家相关运输规定，经充分协商，订立如下条款，以便三方共同遵守:</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="700">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="600">
                    <text fontSize="14">1、上述货物，承运过程中如有短缺、雨淋、损坏等货物损失，均由承运人负责赔偿。</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="700">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="600">
                    <text fontSize="14">2、实行车箱板交货原则，货物装车后，货物安全责任全部转由承运人负责。</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0" sizeType="autoheight">
            <rect color="" name="x6870" width="700" sizeType="autoheight">
                <para align="left" lineSpacing="0" sizeType="autoheight" fillColor="#ff99ff" name="x6870" width="600">
                    <text fontSize="14">3、承运单位必须保证其提供的运输车辆信息真实、准确，如出现货物损失、被骗、被盗等情况时，概由承运单位、承运人负责全额赔偿。</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="700">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="600">
                    <text fontSize="14">4、如运输车辆、司机下落不明，货物灭失，则承运车主和调派单位全额赔偿。</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="200">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="250">
                    <text fontSize="14">委托单位:XXXX集团</text>
                </para>
            </rect>
            <rect color="" name="x6871" width="250">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="200">
                    <text fontSize="14">承运单位:</text>
                    <text fontSize="14">XXXX物流有限公司</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="90">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6872">
                    <text fontSize="14">承运车主:</text>
                </para>
            </rect>
            <rect align="right" color="" name="x6872" width="60" height="40"><img
                    src="http://localhost:8080/api/img/name.bmp"
                    height="40" width="50"/></rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="200">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="250">
                    <text fontSize="14">经办人:办单终端</text>
                </para>
            </rect>
            <rect color="" name="x6871" width="250">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6871" width="200">
                    <text fontSize="14">经办人:</text>
                    <text fontSize="14"> 张三 </text>
                </para>
            </rect>
            <rect color="" name="x6872" width="200">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6872" width="200">
                    <text fontSize="14"></text>
                    <text fontSize="14"></text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="450" height="60">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6870" width="450">
                    <text fontSize="14">电话:</text>
                    <text fontSize="14">13956797208</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="60" height="50">
                <para align="left" lineSpacing="3" fillColor="#ff99ff" name="x6872">
                    <text fontSize="14">指纹:</text>
                </para>
            </rect>
            <rect align="left" color="" name="x6872" width="60" height="60"><img
                    src="http://localhost:8080/api/img/print.bmp"
                    height="50" width="60"/></rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="700">
                <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="700">
                    <text fontSize="12">- - - - - - - - - - - - - - - - - - - - - - - - 门 卫 留 存 - - - - - - - - - - - -
                        - - - - - - - - - - - -
                    </text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6870" width="220">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="220">
                    <text fontSize="12">车辆预约信息:</text>
                    <text fontSize="12">2018-12-23 0:00-24:00</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="100">
                <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="100">
                    <text fontSize="12">运输方式:</text>
                    <text fontSize="12">汽运</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="300">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="300">
                    <text fontSize="12">收货单位:</text>
                    <text fontSize="12"> 新惠利江苏金龙化肥有限公司</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="60">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="60">
                    <text fontSize="12"> 【1-1】</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="200">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="200">
                    <text fontSize="12">配载单号:</text>
                    <text fontSize="12">PZ181223000009</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="150">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="150">
                    <text fontSize="12">承运车号:</text>
                    <text fontSize="12">豫NY3297</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="200">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="200">
                    <text fontSize="12">办单时间:</text>
                    <text fontSize="12">2019-12-03 14:46</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="80">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="80">
                    <text fontSize="12">发货人签字:</text>
                    <text fontSize="12"></text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0">
            <table cellpadding="50" cellspacing="0" height="400" width="700" sizeType="autoheight" name="x347"
                   header="1" cols="75,75,85,75,75,75,75,70,85" rows="">
                <rect col="1" align="center" row="1" colSpan="2" rowSpan="6" name="x347">
                    <para lineSpacing="0">
                        <rect align="left" color="" name="x5246" width="140" height="140"><img
                                src="http://localhost:8080/api/img/qrCode.png"
                                height="140" width="140"/></rect>
                    </para>
                </rect>
                <rect col="3" align="center" row="1" colSpan="5" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12">物料名称</text>
                    </para>
                </rect>
                <rect col="8" align="center" row="1" colSpan="2" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12">吨位</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="2" colSpan="5" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12">粉尘尿素</text>
                    </para>
                </rect>
                <rect col="8" align="center" row="2" colSpan="2" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12">35.00</text>
                    </para>
                </rect>
                <rect col="1" align="center" row="7" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">合计（小写）</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="7" colSpan="7" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">T=35.00</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="3" colSpan="5" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12"></text>
                    </para>
                </rect>
                <rect col="8" align="center" row="3" colSpan="2" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12"></text>
                    </para>
                </rect>
                <rect col="1" align="center" row="7" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">合计（小写）</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="7" colSpan="7" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">T=35.00</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="4" colSpan="5" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12"></text>
                    </para>
                </rect>
                <rect col="8" align="center" row="4" colSpan="2" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12"></text>
                    </para>
                </rect>
                <rect col="1" align="center" row="7" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">合计（小写）</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="7" colSpan="7" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">T=35.00</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="5" colSpan="5" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12"></text>
                    </para>
                </rect>
                <rect col="8" align="center" row="5" colSpan="2" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12"></text>
                    </para>
                </rect>
                <rect col="1" align="center" row="7" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">合计（小写）</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="7" colSpan="7" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">T=35.00</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="6" colSpan="5" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12"></text>
                    </para>
                </rect>
                <rect col="8" align="center" row="6" colSpan="2" rowSpan="1" name="x347">
                    <para align="center">
                        <text fontSize="12"></text>
                    </para>
                </rect>
                <rect col="1" align="center" row="7" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">合计（小写）</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="7" colSpan="7" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">T=35.00</text>
                    </para>
                </rect>
            </table>
        </para>
        <para lineSpacing="8">
            <rect color="" name="x6870" width="700">
                <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6870" width="700">
                    <text fontSize="12">- - - - - - - - - - - - - - - - - - - - - - - - 化 管 留 存 - - - - - - - - - - - -
                        - - - - - - - - - - - -
                    </text>
                </para>
            </rect>
        </para>
        <para lineSpacing="0">
            <rect color="" name="x6872" width="200">
                <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="250">
                    <text fontSize="12">配载单号:</text>
                    <text fontSize="12">PZ181223000009</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="100">
                <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="200">
                    <text fontSize="12">运输方式:</text>
                    <text fontSize="12">汽运</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="150">
                <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="200">
                    <text fontSize="12">承运车号:</text>
                    <text fontSize="12">豫NY3297</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="230">
                <para align="center" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="230">
                    <text fontSize="12">办单时间:</text>
                    <text fontSize="12">2019-12-03 14:46</text>
                    <text fontSize="12"> 【1-1】</text>
                </para>
            </rect>
            <rect color="" name="x6872" width="300">
                <para align="left" lineSpacing="0" fillColor="#ff99ff" name="x6872" width="300">
                    <text fontSize="12"> 收货单位:</text>
                    <text fontSize="12"> 新惠利江苏金龙化肥有限公司</text>
                </para>
            </rect>
        </para>
        <para lineSpacing="1">
            <table cellpadding="0" cellspacing="0" height="400" width="700" sizeType="autoheight" name="x342" header="1"
                   cols="75,75,85,75,75,75,75,70,85" rows="">
                <rect col="1" align="center" row="1" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">ERP单号</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="1" colSpan="3" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">品名</text>
                    </para>
                </rect>
                <rect col="6" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">规格</text>
                    </para>
                </rect>
                <rect col="7" align="center" row="1" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">产品特性</text>
                    </para>
                </rect>
                <rect col="9" align="center" row="1" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">数量</text>
                    </para>
                </rect>
                <rect col="11" align="center" row="1" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">备注</text>
                    </para>
                </rect>
                <rect col="1" align="center" row="2" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontStyle="bold" fontSize="16">181205008</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="2" colSpan="3" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">粉尘尿素</text>
                    </para>
                </rect>
                <rect col="6" align="center" row="2" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">50Kg</text>
                    </para>
                </rect>
                <rect col="7" align="center" row="2" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">白分成</text>
                    </para>
                </rect>
                <rect col="9" align="center" row="2" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">35.00</text>
                    </para>
                </rect>
                <rect col="11" align="center" row="2" colSpan="1" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12"></text>
                    </para>
                </rect>
                <rect col="1" align="center" row="3" colSpan="2" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">合计（小写）</text>
                    </para>
                </rect>
                <rect col="3" align="center" row="3" colSpan="9" rowSpan="1" name="x345">
                    <para align="center">
                        <text fontSize="12">T=35.00</text>
                    </para>
                </rect>
            </table>
        </para>
        <para breakPage="true"/>
        </body>
    </xdoc>
</script>
</body>
</html>